<?php

namespace Drupal\css_modal;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Controller\TitleResolverInterface;
use Drupal\Core\Render\MainContent\MainContentRendererInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Append modal content to the body with the CSS modal template wrapper.
 */
class CssModalRenderer implements MainContentRendererInterface {


  /**
   * The title resolver.
   *
   * @var \Drupal\Core\Controller\TitleResolverInterface
   */
  protected $titleResolver;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a new CssModalRenderer.
   *
   * @param \Drupal\Core\Controller\TitleResolverInterface $title_resolver
   *   The title resolver.
   */
  public function __construct(TitleResolverInterface $title_resolver, RendererInterface $renderer) {
    $this->titleResolver = $title_resolver;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public function renderResponse(array $main_content, Request $request, RouteMatchInterface $route_match) {
    $response = new AjaxResponse();
    $title = isset($main_content['#title']) ? $main_content['#title'] : $this->titleResolver->getTitle($request, $route_match->getRouteObject());
    $main_content['#theme_wrappers']['css_modal'] = [
      'title' => $title,
      'route_match' => $route_match,
    ];
    $content = $this->renderer->renderRoot($main_content);
    $main_content['#attached']['library'][] = 'css_modal/css_modal';
    $response->setAttachments($main_content['#attached']);
    $response->addCommand(new AppendCommand('body', $content));
    $response->addCommand(new InvokeCommand('body', 'addClass', ['modal-open']));
    return $response;
  }

}
