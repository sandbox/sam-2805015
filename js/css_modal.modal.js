(function($) {

  // Create a super basic API for manipulating the modal.
  window.css_modal = {
    close: function() {
      $('.modal').remove();
      $('body').removeClass('modal-open');
    }
  };

  /**
   * Light css modal.
   */
  Drupal.behaviors.cssModal = {
    attach: function(context, settings) {
      $('.modal__close, .modal__overlay', context).once().click(window.css_modal.close);
    }
  };

})(jQuery);
